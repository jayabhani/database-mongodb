let mongoose = require('mongoose')
let myEmail = require('./models/email.js')

const server = '127.0.0.1:27017'
const database = 'schooldb'

class Database{
    constructor(){
        this._connect()
    }
    _connect(){
        mongoose.connect(`mongodb://${server}/${database}`)
        .then(() => {
            console.log('Database connection successful');
        })
        .catch(err => {
            console.error('Database connection error')
        })
    }
}

module.exports = new Database()