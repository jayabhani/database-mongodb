// referencing mongoose
let mongoose = require('mongoose')

// defining the schema
let emailSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        validate: (value) => {
            return validator.isEmail(value)
        }
    }
})

// exporting a model
module.exports = mongoose.model('Email', emailSchema)